import dbConnect from '../../../db/conn';
import { deleteTodo } from '../../../db';

/**
 * @type {import('@sveltejs/kit').RequestHandler}
 */
export async function del({ params: { id } }) {
	await dbConnect();
	const result = await deleteTodo(id);
	return result;
}
