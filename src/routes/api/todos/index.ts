import dbConnect from '../../../db/conn';
import { createTodo, getTodos } from '../../../db';

/**
 * @type {import('@sveltejs/kit').RequestHandler}
 */
export async function get() {
	await dbConnect();
	const todos = await getTodos();
	return {
		body: todos
	};
}

/**
 * @type {import('@sveltejs/kit').RequestHandler}
 */
export async function post({ body }) {
	await dbConnect();
	const todo = await createTodo(JSON.parse(body));
	return {
		body: todo
	};
}
