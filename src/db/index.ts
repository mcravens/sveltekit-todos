import { model, Schema } from 'mongoose';

interface Todo {
	title: string;
	completed?: boolean;
	notes?: string;
}

const TodoSchema = new Schema<Todo>(
	{
		title: {
			type: String,
			required: true
		},
		completed: {
			type: Boolean,
			required: false
		},
		notes: {
			type: String,
			required: false
		}
	},
	{ versionKey: false }
);

const TodoModel = model<Todo>('Todo', TodoSchema);

export async function getTodos(): Promise<Todo[]> {
	const todos = await TodoModel.find();
	return todos;
}

export async function createTodo({ title, completed }: Todo) {
	const { _id } = await TodoModel.create({ title, completed });
	const todo = await TodoModel.findOne({ _id });
	return todo;
}

export async function deleteTodo(id: string): Promise<{ success: boolean }> {
	await TodoModel.deleteOne({ _id: id });
	return {
		success: true
	};
}
